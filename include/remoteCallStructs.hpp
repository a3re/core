#pragma once

#include <cstdint>

namespace RemoteCallStructs {
    typedef enum : uint8_t {
        UNKNOWN_PACKET_TYPE     = 0,
        HANDSHAKE               = 1,
        CALL                    = 2,
        CALLRET                 = 3,
        PING                    = 4,
        PONG                    = 5,
    } PacketType_t;

    typedef struct {
        PacketType_t type;
    } Header_t;

    typedef struct {
        uint32_t id;
        uint64_t timestamp;
        uint32_t size;
    } Call_t;

    typedef struct {
        uint32_t id;
        uint32_t size;
        uint64_t timestamp;
        int32_t ret;
    } CallRet_t;
}