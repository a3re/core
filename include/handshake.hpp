#pragma once

#include <cstdint>

#include <packet.hpp>
#include <node.hpp>

namespace Handshake {
    typedef enum : uint8_t {
        HANDSHAKE_STAGE_UNKNOWN     = 0,
        HANDSHAKE_STAGE_CONNECT     = 1,
        HANDSHAKE_STAGE_EXCHANGE    = 2,
        HANDSHAKE_STAGE_ACCEPT      = 3,
        HANDSHAKE_STAGE_REJECT      = 4,
        HANDSHAKE_STAGE_DISCONNECT  = 5,
    } stage;

    typedef enum : uint8_t {
        HANDSHAKE_NO_ERROR           = 0,
        HANDSHAKE_UNKNOWN_ERROR      = 1,
        HANDSHAKE_RECONNECTION       = 2,
        HANDSHAKE_BAD_TYPE           = 3,
        HANDSHAKE_OUT_OF_SEQUENCE    = 4,
    } error;

    typedef struct {
        Handshake::stage stage;
        Handshake::error error;
    } header;

    typedef struct {
        struct {
            uint8_t major;
            uint8_t minor;
        } version;
        Node::capatibilites_t   caps;
        Node::type_t            type;
    } data;
};