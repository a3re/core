#pragma once

#include <cstdint>
#include <chrono>

#include <utils.hpp>
#include <udp_node.hpp>

class Node {
    public:
        typedef enum : uint8_t {
            NODE_TYPE_UNKNOWN       = 0,
            NODE_TYPE_EXTENSION     = 1,
            NODE_TYPE_SERVER        = 2,
            NODE_TYPE_CLIENT        = 3, // For testing purposes

            NODE_TYPE_INVALID,
            NODE_TYPE_MAX,
        } type_t;

        typedef enum : uint8_t {
            NODE_STATUS_DISCONNECTED    = 0,
            NODE_STATUS_CONNECTING      = 1,
            NODE_STATUS_HANDSHAKING     = 2,
            NODE_STATUS_FINALIZING      = 3,
            NODE_STATUS_CONNECTED       = 4,
            NODE_STATUS_REJECTED        = 5,
            NODE_STATUS_DISCONNECTING   = 6,

            NODE_STATUS_MAX,
        } status_t;

        typedef struct {
            union {
                uint32_t    raw;

                uint32_t    rfe   : 1;  // Remote Function Execution
                uint32_t    res   : 31;
            };
        } capatibilites_t;

        Node::type_t            type    = Node::type_t::NODE_TYPE_UNKNOWN;
        Node::status_t          status  = Node::status_t::NODE_STATUS_DISCONNECTED;
        Node::capatibilites_t   caps;

        Node() : Node(UDPNode::zero_node) {};
        Node(Node::type_t type) : Node(UDPNode::zero_node, type) {};

        Node(UDPNode::node_t node) : Node(node, Node::type_t::NODE_TYPE_UNKNOWN) {};
        Node(UDPNode::node_t node, Node::type_t type) : Node(node, type, Node::status_t::NODE_STATUS_DISCONNECTED) {};
        Node(UDPNode::node_t node, Node::type_t type, Node::status_t status) : node(node), type(type), status(status) {
            this->uid = generateRandomValue(0, 1000000);
            this->attemptsWithoutPong = 0;
        };

        void setGotPing(bool val) {
            if (val)
                this->attemptsWithoutPong = 0;

            this->gotPing = val;
        }

        bool isGotPing() {
            return this->gotPing;
        }

        void addMissingPong() {
            this->attemptsWithoutPong++;
        }

        bool isAvailable() {
            return this->attemptsWithoutPong < 5;
        }

        bool isConnected() {
            return this->status == Node::status_t::NODE_STATUS_CONNECTED;
        }

        UDPNode::node_t getNode() {
            return this->node;
        }

        uint32_t getUID() {
            return this->uid;
        }

        bool operator==(const UDPNode::node_t& other) const {
            return this->node.ip == other.ip && this->node.port == other.port;
        }

        bool operator==(const uint32_t& uid) const {
            return this->uid == uid;
        }

    protected:
        uint32_t                uid;
        UDPNode::node_t         node;
        bool                    gotPing;
        uint32_t                attemptsWithoutPong;
};