#pragma once

#include <vector>
#include <map>
#include <string>
#include <cstring>
#include <functional>
#include <chrono>

class Caller {
	public:
		typedef enum : int32_t {
			NOT_SUPPORTED           = -4,
            INTERNAL_ERROR          = -3,
            NOT_ENOUGH_ARGUMENTS	= -2,
			FUNC_NOT_FOUND      	= -1,
			OK	        			= 0,
		} Status_t;

		typedef struct {
			std::string	out;
			Status_t	ret;
			uint32_t	took;
		} Return_t;

		typedef std::function<Return_t(std::vector<std::string>, void*)> func_t;

		Caller();

		void addFunction(std::string name, func_t proc);

		Return_t process(std::string func, std::vector<std::string> args);
		Return_t process(std::string func, std::vector<std::string> args, void* ptr);

	private:
		std::map<std::string, func_t> handlers;
};