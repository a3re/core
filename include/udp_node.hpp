#pragma once

#include <string>
#include <vector>
#include <cstdlib>

#include <tinyformat.h>

#if defined(___WINDOWS___)
    #include <winsock2.h>
    #include <winsock.h>
    #include <ws2tcpip.h>
    #include <windows.h>
    #pragma comment(lib, "ws2_32.lib")
#elif defined(___UNIX___)
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <arpa/inet.h>
    #include <unistd.h>
    #include <fcntl.h>
    #include <poll.h>
#else
#error "This platform is not supported"
#endif

class UDPNode {
    public:
        struct node {
            std::string ip;
            uint32_t port;

            std::string toString() {
                return tfm::format("%s:%d", this->ip, this->port);
            }
        };

        typedef struct node node_t;

        static UDPNode::node_t zero_node;

        UDPNode(node_t node);
        UDPNode(std::string ip, uint32_t port);
        UDPNode();
        ~UDPNode();

        std::vector<uint8_t> recv(node_t& node, uint32_t timeoutMs);
        std::vector<uint8_t> recv(std::string& ip, uint32_t& port, uint32_t timeoutMs);

        void send(node_t node, std::vector<uint8_t>& data);
        void send(std::string ip, uint32_t port, std::vector<uint8_t>& data);

        void close_connection();
    private:
        bool isServer = false;

        std::string ip;
        uint32_t port;

#if defined(___WINDOWS___)
        SOCKET sockfd = 0;
#else
        int sockfd = 0;
#endif
};