#pragma once

#include <cstring>
#include <cstdint>
#include <queue>

#include <udp_node.hpp>
#include <node.hpp>

typedef std::vector<uint8_t> Raw_t;

class Packet;

template<typename T>
class PacketPiece;

class Packet {
    public:
        Packet() : Packet(UDPNode::zero_node) {};
        Packet(Raw_t raw) : Packet(UDPNode::zero_node, raw) {};

        Packet(UDPNode::node_t node) : Packet(node, Raw_t()) {};
        Packet(UDPNode::node_t node, Raw_t raw) : node(node), raw(raw) {};

        // ~Packet() {
        //     std::cout << tfm::format("Packet destroyed, data size %d", this->raw.size()) << std::endl;
        // }

        template<typename T>
        void push(T data) {
            Raw_t rawData = PacketPiece<T>(data).toRaw();
            this->pushRawData(rawData);
        }

        template<typename T>
        void push(PacketPiece<T>& piece) {
            Raw_t rawData = piece.toRaw();
            this->pushRawData(rawData);
        }

        template<typename T>
        PacketPiece<T> pop() {
            Raw_t rawData = this->popRawData(sizeof(T));
            return PacketPiece<T>(rawData);
        }

        void pushRawData(Raw_t rawData) {
            raw.insert(raw.end(), rawData.begin(), rawData.end());
        }

        Raw_t popRawData(uint32_t size) {
            Raw_t rawData;

            if (size >= raw.size()) {
                rawData = raw;
                raw.clear();
            } else {
                rawData = Raw_t(raw.begin(), raw.begin() + size);
                raw.erase(raw.begin(), raw.begin() + size);
            }

            return rawData;
        }

        Raw_t getRaw() {
            return this->raw;
        }

        UDPNode::node_t getNode() {
            return this->node;
        }

        uint32_t getSize() {
            return this->raw.size();
        }

        void clear() {
            this->raw.clear();
        }
    private:
        UDPNode::node_t node;
        Raw_t raw;
        

};

template<typename T>
class PacketPiece {
    public:
        PacketPiece() {};
        PacketPiece(T data) : data(data) {};
        PacketPiece(Raw_t raw) {
            this->fromRaw(raw);
        }

        // ~PacketPiece() {
        //     std::cout << tfm::format("PacketPiece<%s> destroyed", typeid(T).name()) << std::endl;
        // }

        Raw_t toRaw() {
            Raw_t raw;
            const uint8_t* bytes = reinterpret_cast<const uint8_t*>(&this->data);

            for (size_t i = 0; i < sizeof(T); ++i)
                raw.push_back(bytes[i]);

            return raw;
        }

        void fromRaw(Raw_t raw) {
            std::memcpy(&this->data, raw.data(), sizeof(T));
        }

        T* operator->() {
            return &this->data;
        }

        T& operator*() { 
            return this->data;
        } 
    private:
        T data;
};