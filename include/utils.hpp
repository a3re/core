#pragma once

#include <vector>
#include <string>
#include <cstdint>
#include <random>

#define stringify_param(x) #x
#define stringify(x) stringify_param(x)

std::vector<uint8_t>        convertStringToBytes(const std::string& str);
std::string                 convertBytesToString(const std::vector<uint8_t>& bytes);
std::vector<uint8_t>        convertStringsToBytes(const std::vector<std::string>& strings);
std::vector<std::string>    convertBytesToStrings(const std::vector<uint8_t>& bytes);
int                         generateRandomValue(int x, int y);