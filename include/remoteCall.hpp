#pragma once

#include <string>
#include <queue>
#include <list>
#include <vector>
#include <thread>
#include <mutex>
#include <atomic>
#include <condition_variable>
#include <ctime>

#include <utils.hpp>
#include <caller.hpp>
#include <udp_node.hpp>
#include <packet.hpp>
#include <node.hpp>
#include <handshake.hpp>
#include <remoteCallStructs.hpp>

using namespace RemoteCallStructs;

class RemoteCall {
    public:
        typedef struct {
            uint32_t from;
            uint32_t id;
            uint32_t ret;
            uint32_t took;
            std::string out;
        } callret_pt;

        typedef std::shared_ptr<Packet> SharedPacket;

        RemoteCall(Node::type_t type, UDPNode::node_t node) : RemoteCall(NULL, type, node) {};
        RemoteCall(Caller* caller, Node::type_t type, UDPNode::node_t node);
        ~RemoteCall();

        std::string getVersion();

        uint32_t connect(UDPNode::node_t node);
        void disconnect(uint32_t uid);
        
        bool isDisconnected(uint32_t val);
        bool isConnected(uint32_t val);
        bool isRejected(uint32_t val);

        uint32_t call(uint32_t uid, std::string func);
        uint32_t call(uint32_t uid, std::string func, std::vector<std::string> args);

        Caller::Return_t getCallRet(uint32_t uid, uint32_t calluid, uint32_t timeout);

    private:
        void send_async(SharedPacket packet);
        void send(SharedPacket packet);

        std::list<Node>::iterator addNode(UDPNode::node_t node);

        template<typename T>
        std::list<Node>::iterator getNode(T val) {
            return std::find(assignedNodes.begin(), assignedNodes.end(), val);
        }

        bool delNode(UDPNode::node_t node);
        bool isValidNode(std::list<Node>::iterator node);

        SharedPacket processHandshake(std::list<Node>::iterator node, SharedPacket packet);
        SharedPacket processCall(std::list<Node>::iterator node, SharedPacket packet);
        SharedPacket processCallRet(std::list<Node>::iterator node, SharedPacket packet);

    protected:
        UDPNode node;
        Caller* caller;

        Node::type_t type;

        std::list<Node> assignedNodes;
        std::mutex* assignedNodesMutex;

        std::vector<callret_pt> callret_queue;
        std::queue<SharedPacket> send_queue;
        std::queue<SharedPacket> recv_queue;

        std::atomic<bool> _running;

        std::thread _sheduler_thread;
        std::thread _send_thread;
        std::thread _recv_thread;
        std::thread _garbage_collector;

        std::mutex* _send_thread_mutex;
        std::condition_variable _send_thread_cv;

        std::mutex* _recv_thread_mutex;
        std::condition_variable _recv_thread_cv;

        void packet_scheduler();
        void send_handler();
        void recv_handler();
        void garbage_collector();
};