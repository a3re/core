#include <caller.hpp>

#include <tinyformat.h>

#define MODULE_NAME "Caller"
#include <log.hpp>

Caller::Caller() {}

void Caller::addFunction(std::string name, func_t func) {
    if (auto iter = this->handlers.find(name); iter != this->handlers.end())
        return;

    LOG_INFO("Adding function: %s (at 0x%016X)", name, (uint64_t)((void*&)func));
    this->handlers[name] = func;
}

Caller::Return_t Caller::process(std::string func, std::vector<std::string> args) {
    return this->process(func, args, NULL);
}

Caller::Return_t Caller::process(std::string func, std::vector<std::string> args, void* ptr) {
    auto iter = this->handlers.find(func);
    if (iter == this->handlers.end())
        return {
            .out = "Function not found",
            .ret = FUNC_NOT_FOUND,
            .took = 0,
        };

    auto start_point = std::chrono::steady_clock::now();
    Caller::Return_t ret = iter->second(args, ptr);
    auto end_point = std::chrono::steady_clock::now();

    ret.took = (uint32_t)std::chrono::duration_cast<std::chrono::microseconds>(end_point - start_point).count();

    return ret;
}