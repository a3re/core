#include <remoteCall.hpp>

#include <tinyformat.h>

#define MODULE_NAME "remoteCall"
#include <log.hpp>

RemoteCall::RemoteCall(Caller* caller, Node::type_t type, UDPNode::node_t node) : caller(caller), type(type), node(node) {
    this->_running = true;

    this->assignedNodesMutex = new std::mutex();

    this->_send_thread_mutex = new std::mutex();
    this->_recv_thread_mutex = new std::mutex();

    this->_sheduler_thread = std::thread(&RemoteCall::packet_scheduler, this);
    this->_send_thread = std::thread(&RemoteCall::send_handler, this);
    this->_recv_thread = std::thread(&RemoteCall::recv_handler, this);
    this->_garbage_collector = std::thread(&RemoteCall::garbage_collector, this);
}

RemoteCall::~RemoteCall() {
    for (auto it = this->assignedNodes.begin(); it != this->assignedNodes.end(); it++) {
        std::lock_guard<std::mutex> lock(*this->assignedNodesMutex);

        uint32_t uid = it->getUID();
        this->disconnect(uid);

        uint32_t attempts = 0;
        while (this->isDisconnected(uid) && attempts < 5) {
            std::this_thread::yield();
            std::this_thread::sleep_for(std::chrono::milliseconds(10));

            attempts++;
        }
    }

    this->node.close_connection();

    this->_running = false;

    this->_recv_thread_cv.notify_all();
    this->_send_thread_cv.notify_all();

    if (this->_sheduler_thread.joinable())
        this->_sheduler_thread.join();

    if (this->_send_thread.joinable())
        this->_send_thread.join();

    if (this->_recv_thread.joinable())
        this->_recv_thread.join();

    if (this->_garbage_collector.joinable())
        this->_garbage_collector.join();

    delete this->assignedNodesMutex;

    delete this->_send_thread_mutex;
    delete this->_recv_thread_mutex;
}

std::string RemoteCall::getVersion() {
    return tfm::format("%s.%s", REMOTECALL_SUBSYSTEM_VERSION_MAJOR, REMOTECALL_SUBSYSTEM_VERSION_MINOR);
}

bool RemoteCall::isDisconnected(uint32_t uid) {
    auto node = this->getNode(uid);
            
    if (!this->isValidNode(node))
        return false;

    return node->status == Node::status_t::NODE_STATUS_DISCONNECTED;
}

bool RemoteCall::isConnected(uint32_t uid) {
    auto node = this->getNode(uid);
            
    if (!this->isValidNode(node))
        return false;

    return node->status == Node::status_t::NODE_STATUS_CONNECTED;
}

bool RemoteCall::isRejected(uint32_t uid) {
    auto node = this->getNode(uid);

    if (!this->isValidNode(node))
        return false;

    return node->status == Node::status_t::NODE_STATUS_REJECTED;
}

uint32_t RemoteCall::connect(UDPNode::node_t _node) {
    std::lock_guard<std::mutex> lock(*this->assignedNodesMutex);

    LOG_INFO("Start connecting");

    SharedPacket _packet = std::make_shared<Packet>(_node);

    _packet->push<Header_t>({ 
        .type = HANDSHAKE,
    });

    _packet->push<Handshake::header>({
        .stage = Handshake::stage::HANDSHAKE_STAGE_CONNECT,
        .error = Handshake::error::HANDSHAKE_NO_ERROR,
    });

    auto node = this->addNode(_node);
    node->status = Node::status_t::NODE_STATUS_CONNECTING;

    this->send_async(_packet);

    return node->getUID();
}

void RemoteCall::disconnect(uint32_t uid) {
    LOG_INFO("Start disconnecting");

    auto node = this->getNode(uid);

    if (!this->isValidNode(node))
        return;

    SharedPacket _packet = std::make_shared<Packet>(node->getNode());

    _packet->push<Header_t>({ 
        .type = HANDSHAKE,
    });

    _packet->push<Handshake::header>({
        .stage = Handshake::stage::HANDSHAKE_STAGE_DISCONNECT,
        .error = Handshake::error::HANDSHAKE_NO_ERROR,
    });

    node->status = Node::status_t::NODE_STATUS_DISCONNECTING;
    this->send(_packet);
}

uint32_t RemoteCall::call(uint32_t uid, std::string func) {
    std::vector<std::string> args;
    return this->call(uid, func, args);
}

uint32_t RemoteCall::call(uint32_t uid, std::string func, std::vector<std::string> args) {
    std::lock_guard<std::mutex> lock(*this->assignedNodesMutex);

    auto node = this->getNode(uid);

    if (!this->isValidNode(node))
        return 0;

    if (node->status != Node::status_t::NODE_STATUS_CONNECTED || !node->caps.rfe)
        return 0;

    LOG_DEBUG("Initiating remote call: %s", func);

    auto time_point = std::chrono::steady_clock::now();
    uint64_t time_point_value = std::chrono::time_point_cast<std::chrono::microseconds>(time_point).time_since_epoch().count();

    uint32_t _call_id = generateRandomValue(1, 100000);
    std::vector<uint8_t> bfunc = convertStringToBytes(func);
    std::vector<uint8_t> payload = convertStringsToBytes(args);

    SharedPacket packet_call = std::make_shared<Packet>(node->getNode());

    packet_call->push<Header_t>({ 
        .type = CALL,
    });

    packet_call->push<Call_t>({
        .id         = _call_id,
        .timestamp  = time_point_value,
        .size       = (uint32_t)payload.size() + (uint32_t)bfunc.size(),
    });

    packet_call->pushRawData(bfunc);
    packet_call->pushRawData(payload);

    LOG_DEBUG("[0x%08X] Send call packet", _call_id);
    this->send_async(packet_call);

    return _call_id;
}

Caller::Return_t RemoteCall::getCallRet(uint32_t uid, uint32_t calluid, uint32_t timeout) {
    uint32_t attempts = 0;

    while (true) {
        auto it = std::find_if(callret_queue.begin(), callret_queue.end(), [uid, calluid](const callret_pt& element) {
            return element.from == uid && element.id == calluid;
        });

        if (it != callret_queue.end()) {
            Caller::Return_t ret = {
                .out = it->out,
                .ret = (Caller::Status_t)it->ret,
                .took = it->took,
            };

            callret_queue.erase(it);
            return ret;
        }

        if (attempts > timeout / 10)
            break;

        std::this_thread::yield();
        std::this_thread::sleep_for(std::chrono::milliseconds(10));

        attempts++;
    }

    return {
        .out = "No response",
        .ret = Caller::Status_t::INTERNAL_ERROR,
        .took = timeout * 1000,
    };
}

void RemoteCall::send_async(SharedPacket packet) {
    this->send_queue.push(packet);
    this->_send_thread_cv.notify_all();
}

void RemoteCall::send(SharedPacket packet) {
    std::vector<uint8_t> data = packet->getRaw();
    this->node.send(packet->getNode(), data);
}

//TODO: Need to add some kind of sessions or thread-per-packet(?) as solution
void RemoteCall::packet_scheduler() {
    LOG_DEBUG("Starting %s's thread", __func__);

    while (this->_running) {
        while (!this->recv_queue.empty()) {
            std::lock_guard<std::mutex> lock(*this->assignedNodesMutex);

            SharedPacket packet = this->recv_queue.front();
            this->recv_queue.pop();

            SharedPacket ret_packet = std::make_shared<Packet>(packet->getNode());
            PacketPiece<Header_t> _header = packet->pop<Header_t>();

            auto node = this->addNode(packet->getNode());

            LOG_DEBUG("Got packet from node %s (uid 0x%08X, type %d)", node->getNode().toString(), node->getUID(), (uint32_t)_header->type);

            switch (_header->type) {
                case HANDSHAKE:
                    ret_packet = this->processHandshake(node, packet);
                    break;

                case CALL:
                    if (!node->isConnected()) break;

                    ret_packet = this->processCall(node, packet);
                    break;

                case CALLRET:
                    if (!node->isConnected()) break;

                    ret_packet = this->processCallRet(node, packet);
                    break;

                case PING:
                    if (!node->isConnected()) break;

                    LOG_DEBUG("Got PING from %s, sending PONG", node->getNode().toString());
                    ret_packet->push<Header_t>({ 
                        .type = PONG,
                    });
                    node->setGotPing(true);
                    break;

                case PONG:
                    if (!node->isConnected()) break;

                    LOG_DEBUG("Got PONG from %s", node->getNode().toString());
                    node->setGotPing(true);
                    break;
            };

            if (ret_packet->getSize() > 0)
                this->send(ret_packet);
        }

        if (this->_running) {
            std::unique_lock<std::mutex> lock(*this->_recv_thread_mutex);
            this->_recv_thread_cv.wait(lock);
        }
    }

    LOG_DEBUG("Stopping %s's thread", __func__);
}

RemoteCall::SharedPacket RemoteCall::processHandshake(std::list<Node>::iterator node, SharedPacket packet) {
    SharedPacket ret_packet = std::make_shared<Packet>(packet->getNode());

    ret_packet->push<Header_t>({ 
        .type = HANDSHAKE,
    });

    PacketPiece<Handshake::header> _header = packet->pop<Handshake::header>();

    LOG_DEBUG("Node %s (stage %d, status %d, error %d)", node->getNode().toString(), (uint8_t)_header->stage, (uint8_t)node->status, (uint8_t)_header->error);

    if (_header->error == Handshake::error::HANDSHAKE_RECONNECTION)
        LOG_INFO("DETECTED RECONNECTION WITH NODE %s!!!", node->getNode().toString());

    switch (_header->stage) {
        case Handshake::stage::HANDSHAKE_STAGE_CONNECT:
            LOG_INFO("Node %s has started to establish a connection...", node->getNode().toString());

            if (node->status != Node::status_t::NODE_STATUS_DISCONNECTED && node->status != Node::status_t::NODE_STATUS_REJECTED)
                _header->error = Handshake::error::HANDSHAKE_RECONNECTION;
            else
                _header->error = Handshake::error::HANDSHAKE_NO_ERROR;

            _header->stage = Handshake::stage::HANDSHAKE_STAGE_EXCHANGE;

            node->status = Node::status_t::NODE_STATUS_HANDSHAKING;
            ret_packet->push(_header);
            break;

        case Handshake::stage::HANDSHAKE_STAGE_EXCHANGE:
             switch (node->status) {
                case Node::status_t::NODE_STATUS_CONNECTING:
                    node->status = Node::status_t::NODE_STATUS_FINALIZING;
                    _header->stage = Handshake::stage::HANDSHAKE_STAGE_EXCHANGE;

                    ret_packet->push(_header);
                    goto _pack_data;
                    break;

                case Node::status_t::NODE_STATUS_FINALIZING:
                case Node::status_t::NODE_STATUS_HANDSHAKING: {
                    PacketPiece<Handshake::data> _data = packet->pop<Handshake::data>();

                    LOG_INFO("[%s] Node %s has version %d.%d", this->type == Node::type_t::NODE_TYPE_SERVER ? "SERVER" : "OTHER", node->getNode().toString(), _data->version.major, _data->version.minor);
                    
                    std::string caps;
                    if (_data->caps.raw == 0) {
                        caps = "None";
                    } else {
                        if (_data->caps.rfe)
                            caps += "RFE ";
                    }

                    LOG_INFO("Node capatibilites: %s", caps);

                    node->caps = _data->caps;
                    _header->error = Handshake::error::HANDSHAKE_NO_ERROR;

                    if (node->status == Node::status_t::NODE_STATUS_HANDSHAKING) {
                        node->status = Node::status_t::NODE_STATUS_FINALIZING;
                        _header->stage = Handshake::stage::HANDSHAKE_STAGE_EXCHANGE;
                        ret_packet->push(_header);
                    } else {
                        _header->stage = Handshake::stage::HANDSHAKE_STAGE_ACCEPT;
                        ret_packet->push(_header);
                        break;
                    }
                }

                _pack_data:
                    ret_packet->push<Handshake::data>({
                        .version = { 
                            .major = REMOTECALL_SUBSYSTEM_VERSION_MAJOR,
                            .minor = REMOTECALL_SUBSYSTEM_VERSION_MINOR,
                        },
                        .caps = { 
                            .rfe = this->caller != NULL,
                        },
                        .type = this->type,
                    });
                    break;

                default:
                    _header->error = Handshake::error::HANDSHAKE_OUT_OF_SEQUENCE;
                    _header->stage = Handshake::stage::HANDSHAKE_STAGE_REJECT;
                    node->status = Node::status_t::NODE_STATUS_DISCONNECTED;

                    ret_packet->push(_header);
            };
            break;

        case Handshake::stage::HANDSHAKE_STAGE_ACCEPT:
            if (this->type == Node::type_t::NODE_TYPE_SERVER) {
                _header->error = Handshake::error::HANDSHAKE_NO_ERROR;
                _header->stage = Handshake::stage::HANDSHAKE_STAGE_ACCEPT;
                ret_packet->push(_header);
            }

            LOG_INFO("Connection established with node %s (caps 0x%08X)", node->getNode().toString(), node->caps.raw);
            node->status = Node::status_t::NODE_STATUS_CONNECTED;
            break;

        case Handshake::stage::HANDSHAKE_STAGE_REJECT:
            LOG_INFO("Connection rejected with node %s rejected with error code %d", node->getNode().toString(), (uint8_t)_header->error);
            node->status = Node::status_t::NODE_STATUS_REJECTED;
            break;

        case Handshake::stage::HANDSHAKE_STAGE_DISCONNECT:
            switch (node->status) {
                case Node::status_t::NODE_STATUS_CONNECTED:
                    LOG_INFO("Got disconnection request from node %s", node->getNode().toString());
                    node->status = Node::status_t::NODE_STATUS_DISCONNECTED;

                    _header->error = Handshake::error::HANDSHAKE_NO_ERROR;
                    _header->stage = Handshake::stage::HANDSHAKE_STAGE_DISCONNECT;
                    ret_packet->push(_header);
                    break;

                case Node::status_t::NODE_STATUS_DISCONNECTING:
                    LOG_INFO("Node %s confirmed disconnection", node->getNode().toString());
                    node->status = Node::status_t::NODE_STATUS_DISCONNECTED;
                    break;
            }
            break;
    }

    if (ret_packet->getSize() == 1)
        ret_packet->clear();

    return ret_packet;
}

RemoteCall::SharedPacket RemoteCall::processCall(std::list<Node>::iterator node, SharedPacket packet) {
    SharedPacket ret_packet = std::make_shared<Packet>(packet->getNode());

    ret_packet->push<Header_t>({ 
        .type = CALLRET,
    });

    PacketPiece<Call_t> _call = packet->pop<Call_t>();

    std::vector<uint8_t> payload = packet->popRawData(_call->size);
    std::vector<std::string> args = convertBytesToStrings(payload);

    std::string func = args[0];
    args.erase(args.begin());

    LOG_DEBUG("[0x%08X] Execute function %s with %d arguments", _call->id, func, args.size());

    Caller::Return_t ret = this->caller->process(func, args, this);

    payload = convertStringToBytes(ret.out);

    ret_packet->push<CallRet_t>({
        .id = _call->id,
        .size = (uint32_t)ret.out.size(),
        .timestamp = _call->timestamp,
        .ret = ret.ret,
    });

    ret_packet->pushRawData(payload);

    return ret_packet;
}

RemoteCall::SharedPacket RemoteCall::processCallRet(std::list<Node>::iterator node, SharedPacket packet) {
    PacketPiece<CallRet_t> _callret = packet->pop<CallRet_t>();

    std::vector<uint8_t> payload = packet->popRawData(_callret->size);

    std::string out = convertBytesToString(payload);

    auto time_point = std::chrono::steady_clock::now();
    uint64_t time_point_value = std::chrono::time_point_cast<std::chrono::microseconds>(time_point).time_since_epoch().count();
    uint32_t took = time_point_value - _callret->timestamp;

    this->callret_queue.emplace_back(callret_pt{
        .from   = node->getUID(),
        .id     = _callret->id,
        .ret    = (uint32_t)_callret->ret,
        .took   = took,
        .out    = out,
    });

    LOG_DEBUG("[0x%08X] Remote call success (took %d us)", _callret->id, took);

    return std::make_shared<Packet>();
}

void RemoteCall::send_handler() {
    LOG_DEBUG("Starting %s's thread", __func__);

    while (this->_running) {
        while (!this->send_queue.empty()) {     
            SharedPacket packet = this->send_queue.front();
            this->send(packet);
            this->send_queue.pop();
        }

        if (this->_running) {
            std::unique_lock<std::mutex> lock(*this->_send_thread_mutex);
            this->_send_thread_cv.wait(lock);
        }
    }

    LOG_DEBUG("Stopping %s's thread", __func__);
}

void RemoteCall::recv_handler() {
    LOG_DEBUG("Starting %s's thread", __func__);

    while (this->_running) {
        UDPNode::node_t _node;
        std::vector<uint8_t> data = this->node.recv(_node, 1000);

        if (data.size()) {
            SharedPacket packet = std::make_shared<Packet>(_node, data);
            this->recv_queue.push(packet);
            this->_recv_thread_cv.notify_all();
        }
    }

    LOG_DEBUG("Stopping %s's thread", __func__);
}

void RemoteCall::garbage_collector() {
    LOG_DEBUG("Starting %s's thread", __func__);

    while (this->_running) {
        for (auto it = this->assignedNodes.begin(); it != this->assignedNodes.end(); it++) {
            std::lock_guard<std::mutex> lock(*this->assignedNodesMutex);

            // std::cout << tfm::format("Node %d [%s] with type %d on state %d", it->getUID(), it->getNode().toString(), (uint8_t)it->type, (uint8_t)it->status) << std::endl;
            std::string cause = "unknown";

            if (!it->isGotPing())
                it->addMissingPong();

            if (!it->isAvailable()) {
                cause = "inactivity";
            } else if (it->status == Node::status_t::NODE_STATUS_DISCONNECTED) {
                cause = "disconnection";
            } else if (it->status == Node::status_t::NODE_STATUS_REJECTED) {
                cause = "rejection";
            } else {
                SharedPacket ping = std::make_shared<Packet>(it->getNode());
                ping->push<Header_t>({ 
                    .type = PING,
                });

                it->setGotPing(false);
                LOG_DEBUG("Sending PING to %s", it->getNode().toString());
                this->send(ping);
                continue;
            }

            LOG_DEBUG("Killing node %d [%s] due to %s", it->getUID(), it->getNode().toString(), cause);
            // Fucking safe removing BITCH!!!! Don't change this 2 lines. Never.
            auto del = it++;
            this->delNode(del->getNode());
        }

        std::this_thread::yield();
        std::this_thread::sleep_for(std::chrono::seconds(5));
    };

    LOG_DEBUG("Stopping %s's thread", __func__);
}

std::list<Node>::iterator RemoteCall::addNode(UDPNode::node_t node) {
    bool locked = this->assignedNodesMutex->try_lock();

    auto it = this->getNode(node);

    if (this->isValidNode(it))
        goto _success;

    assignedNodes.emplace_front(node);
    it = assignedNodes.begin();

_success:
    if (locked)
        this->assignedNodesMutex->unlock();

    return it;
}

bool RemoteCall::delNode(UDPNode::node_t node) {
    bool locked = this->assignedNodesMutex->try_lock();
    bool res = true;
    auto it = this->getNode(node);

    if (this->isValidNode(it))
        assignedNodes.erase(it);
    else
        res = false;

    if (locked)
        this->assignedNodesMutex->unlock();

    return res;
}

bool RemoteCall::isValidNode(std::list<Node>::iterator node) {
    return node != assignedNodes.end();
}