#include <utils.hpp>

std::vector<uint8_t> convertStringToBytes(const std::string& str) {
    std::vector<uint8_t> result;

    for (char c : str)
        result.push_back((uint8_t)c);

    result.push_back('\0');
    return result;
}

std::string convertBytesToString(const std::vector<uint8_t>& bytes) {
    std::string result;

    for (uint8_t byte : bytes) {
        if (byte == '\0')
            break;
        
        result += (char)byte;
    }

    return result;
}


std::vector<uint8_t> convertStringsToBytes(const std::vector<std::string>& strings) {
    std::vector<uint8_t> result;

    for (const std::string& str : strings) {
        for (char c : str)
            result.push_back((uint8_t)c);

        result.push_back('\0');
    }

    return result;
}

std::vector<std::string> convertBytesToStrings(const std::vector<uint8_t>& bytes) {
    std::vector<std::string> result;
    std::string current_string;

    for (uint8_t byte : bytes) {
        if (byte == '\0') {
            result.push_back(current_string);
            current_string.clear();
        } else {
            current_string += (char)byte;
        }
    }

    if (!current_string.empty())
        result.push_back(current_string);

    return result;
}

std::vector<std::string> split_string(const std::string& str, char delim) {
    std::vector<std::string> result;
    std::string current_string;

    for (char c : str) {
        if (c == delim) {
            result.push_back(current_string);
            current_string.clear();
        } else {
            current_string += c;
        }
    }

    return result;
}

int generateRandomValue(int x, int y) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<int> distribution(x, y);
    return distribution(gen);
}
