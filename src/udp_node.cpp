#include <udp_node.hpp>

#define MODULE_NAME "udp_node"
#include <log.hpp>

const int MAX_BUFFER_SIZE = 1024 * 10;

UDPNode::node_t UDPNode::zero_node = { "0.0.0.0", 0 };

UDPNode::UDPNode(node_t node) : UDPNode(node.ip, node.port) {};
UDPNode::UDPNode(std::string ip, uint32_t port) : isServer(true), ip(ip), port(port) {
    #if defined(___WINDOWS___)
        WSADATA wsaData;
        if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
            LOG_INFO("WSAStartup failed");
    #endif

    this->sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (this->sockfd < 0)
        throw std::runtime_error("failed to create socket");

    sockaddr_in serverAddr;
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(port);
    serverAddr.sin_addr.s_addr = inet_addr(ip.c_str());

    if (bind(this->sockfd, (struct sockaddr*)&serverAddr, sizeof(serverAddr)))
        throw std::runtime_error("bind failed");

#if defined(___WINDOWS___)
    u_long mode = 1;
    ioctlsocket(this->sockfd, FIONBIO, &mode);
#elif defined(___UNIX___)
    int flags = fcntl(this->sockfd, F_GETFL, 0);
    fcntl(this->sockfd, F_SETFL, flags | O_NONBLOCK);
#endif

}


UDPNode::UDPNode() : isServer(false) {
    #if defined(___WINDOWS___)
        WSADATA wsaData;
        if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
            LOG_INFO("WSAStartup failed");
    #endif

    this->sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (this->sockfd < 0)
        throw std::runtime_error("failed to create socket");
}

std::vector<uint8_t> UDPNode::recv(node_t& node, uint32_t timeoutMs) {
    return this->recv(node.ip, node.port, timeoutMs);
}

std::vector<uint8_t> UDPNode::recv(std::string& ip, uint32_t& port, uint32_t timeoutMs) {
    sockaddr_in addr;
    int addrLen = sizeof(addr);

    std::vector<uint8_t> receivedData(MAX_BUFFER_SIZE);

    struct pollfd pfd_read;

    pfd_read.fd = this->sockfd;
    pfd_read.events = POLLIN;


#if defined(___WINDOWS___)
    int result = WSAPoll(&pfd_read, 1, timeoutMs);
#elif defined(___UNIX___)
    int result = poll(&pfd_read, 1, timeoutMs);
#endif

    if (result <= 0) {
        receivedData.clear();
        return receivedData;
    }

    uint32_t bytesRead = recvfrom(this->sockfd, (char*)receivedData.data(), (int)receivedData.size(), 0, (struct sockaddr*)&addr, (socklen_t*)&addrLen);

    char ipBuffer[INET_ADDRSTRLEN];
#if defined(___WINDOWS___)
    char* tmp = inet_ntoa(addr.sin_addr);
    memcpy(ipBuffer, tmp, INET_ADDRSTRLEN);
#else
    inet_ntop(AF_INET, &(addr.sin_addr), ipBuffer, INET_ADDRSTRLEN);
#endif
    ip = ipBuffer;
    port = ntohs(addr.sin_port);

    if (bytesRead == -1) {
        receivedData.clear();
    } else {
        receivedData.resize(bytesRead);
    }

    return receivedData;
}

void UDPNode::send(node_t node, std::vector<uint8_t>& data) {
    this->send(node.ip, node.port, data);
}

void UDPNode::send(std::string ip, uint32_t port, std::vector<uint8_t>& data) {
    sockaddr_in addr;
    int addrLen = sizeof(addr);

    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = inet_addr(ip.c_str());

    uint32_t bytesSent = sendto(this->sockfd, (char*)data.data(), (int)data.size(), 0, (struct sockaddr*)&addr, addrLen);
}

void UDPNode::close_connection() {
    #if defined(___WINDOWS___)
        if (this->sockfd) closesocket(this->sockfd);
        WSACleanup();
    #else
        if (this->sockfd) close(this->sockfd);
    #endif
};

UDPNode::~UDPNode() {
    this->close_connection();
}